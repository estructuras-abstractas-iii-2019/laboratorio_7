# Laboratorio 7: Colas y pilas

## Estudiantes:
```
Roberto Acevedo Mora
Alexander Calderón Torres
```

## Instrucciones de compilación:
Una vez descargado el código, ubíquese en la respectiva carpeta:
```
>>cd {PATH}/lab7
```
Seguidamente, ejecute el make:
```
>>make
```

## Para ejecutar el programa:
```
./bin/lab7
```

## Para generar la documentación de Doxygen:
Ubíquese en la carpeta principal:
```
>>cd {PATH}/proyecto0
```

Compile el archivo Doxyfile:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd docs/latex
>>make
```
Esto generará el archivo refman.pdf con la documentación generada por doxygen.
