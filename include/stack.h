#pragma once
/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 18/02/2020
 * 
 * @file nodo.h
 * @brief Definición de la clase Nodo.
 */
#include "nodo.h"

/**
 * @class Stack
 * @brief Esta clase implementa una pila y sus operaciones de push y pop
 */
class Stack
{
public:

   /** 
     * @brief Constructor por defecto.
     */
	Stack();

   /** 
     * @brief Constructor personalizado
     */
	Stack(char datoInicial);

   /** 
     * @brief Destructor por defecto.
     */
	~Stack();

   /** 
     * @brief Método push que inserta, de la forma que corresponde a las colas, un nuevo nodo
     */
	void push(char dato);

   /** 
     * @brief Método que devuelve el dato Char almacenado en el nodo y lo reubica
     */
	char popRead();

private:
	Nodo* puntero;
};