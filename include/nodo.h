#pragma once
/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 18/02/2020
 * 
 * @file nodo.h
 * @brief Definición de la clase Nodo.
 */
#include <iostream>


/**
 * @class Nodo
 * @brief Esta clase modela los nodos utilizados posteriormente por la pila y la cola
 */
class Nodo
{
public:

   /** 
     * @brief Constructor por defecto.
     */
	Nodo();
   
   /** 
     * @brief Destructor por defecto.
     */
	~Nodo();
	
   /** 
     * @brief Método que devuelve el dato Char almacenado en el nodo
     */
	char getDato();

   /** 
     * @brief Método que permite guardar un dato Char en el nodo
     */
	void setDato(char dato);

   /** 
     * @brief Método que devuelve un puntero al nodo siguiente
     */
	Nodo* getSiguiente();

   /** 
     * @brief Método que permite definir cuál es el nodo siguiente al actual
     */
	void setSiguiente(Nodo* next);

private:
	char dato;
	Nodo* siguiente;
};
