#pragma once
/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 18/02/2020
 * 
 * @file nodo.h
 * @brief Definición de la clase Nodo.
 */
#include "nodo.h"

/**
 * @class Queue
 * @brief Esta clase implementa una cola y sus operaciones de push y pop
 */
class Queue
{
public:

   /** 
     * @brief Constructor por defecto.
     */
	Queue();

   /** 
     * @brief Constructor personalizado
     */
	Queue(char datoInicial);

   /** 
     * @brief Destructor por defecto.
     */
	~Queue();

   /** 
     * @brief Método push que inserta, de la forma que corresponde a las colas, un nuevo nodo
     */
	void push(char dato);

   /** 
     * @brief Método que devuelve el dato Char almacenado en el nodo head, lo saca de la cola y lo reubica
     */
	char popReadHead();

   /** 
     * @brief Método que devuelve el dato Char almacenado en el nodo tail, lo saca de la cola y lo reubica
     */
	char popReadTail();

private:
	Nodo* head;
	Nodo* tail;
};