#include "stack.h"
#include "queue.h"
#include <string>
#include <iostream>

using namespace std;

bool verificarParentesis(string texto){
	int parentIzq=0;
	int parentDer=0;

	Stack* pila = new Stack();

	for (unsigned int i = 0; i < texto.length(); ++i){
		pila->push(texto.at(i));
	}

	for (unsigned int i = 0; i < texto.length(); ++i)
	{
		char popped=pila->popRead();
		if (popped=='('){
			parentIzq++;
		}
		if (popped==')'){
			parentDer++;
		}
	}

	if (parentDer==parentIzq){return true;}
	else{return false;}
}

bool verificarPalindromo(string palabra){
	Queue* cola = new Queue();

	for (unsigned int i = 0; i < palabra.length(); ++i){
		cola->push(palabra.at(i));
	}
	
	for (unsigned int i = 0; i < palabra.length()/2; ++i){
		if (cola->popReadHead()!=cola->popReadTail()){
			return false;
		}
	}

	return true;
}



int main(int argc, char const *argv[])
{
	string textoAVerificar;

	cout << "Escriba un texto para comprobar sus paréntesis: " << endl<<endl;

	cin >> textoAVerificar;

	if(verificarParentesis(textoAVerificar)){
		cout<<endl<<"Los paréntesis dentro del texto están correctamente balanceados."<<endl<<endl;
	} else {
		cout<<endl<<"Los paréntesis están desbalanceados, la sintaxis del texto no es la correcta"<<endl<<endl;
	}
	

	string palabra;



	cout << "Escriba una palabra para comprobar si es un palíndromo: ";
	cin>>palabra;

	if(verificarPalindromo(palabra)){
		cout<<endl<<"Sí es un palíndromo"<<endl<<endl;
	} else {
		cout<<endl<<"No es un palíndromo"<<endl<<endl;
	}


	return 0;
}