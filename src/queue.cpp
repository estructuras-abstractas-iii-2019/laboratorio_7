#include "queue.h"

Queue::Queue(){
	this->head=NULL;
	this->tail=NULL;
}

Queue::Queue(char datoInicial){

	this->head=NULL;
	this->tail=NULL;
}

Queue::~Queue(){}

void Queue::push(char dato){
	Nodo* nuevo=new Nodo();

	nuevo -> setDato(dato);
	nuevo -> setSiguiente(NULL);

	if (this -> head==NULL && this -> tail==NULL){
		this -> head=nuevo;
	} else {
		this -> tail -> setSiguiente(nuevo);
	}

	this->tail=nuevo;
}

char Queue::popReadHead(){
	char dato;
	if (this->head != this->tail){
		dato=this->head->getDato();
		this->head=this->head->getSiguiente();
	}
	return dato;
}

char Queue::popReadTail(){
	char dato;
	Nodo* temporal=this->head;
	dato=this->tail->getDato();

	while (temporal->getSiguiente()!=this->tail && temporal!=this->tail){
		temporal=temporal->getSiguiente();
	}

	this->tail=temporal;
	
	return dato;
}