#include "stack.h"

Stack::Stack(){
	this->puntero= new Nodo();
	this->puntero ->setSiguiente(NULL);
}

Stack::Stack(char datoInicial){
	this->puntero= new Nodo();
	this->puntero->setDato(datoInicial);
	this->puntero->setSiguiente(NULL);
}

void Stack::push(char dato){
	Nodo* nuevo = new Nodo();
	nuevo->setDato(dato);
	nuevo->setSiguiente(this->puntero);
	this->puntero=nuevo;
}

char Stack::popRead(){
	char dato;
	dato=this->puntero->getDato();
	this->puntero=this->puntero->getSiguiente();
	return dato;
}